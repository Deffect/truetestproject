package com.company;

public class User {

    private String name;
    private String surname;
    private String position;
    private String address;


    public User(String strData) {
        String[] arr = strData.split(" ");
        setName(arr[0]);
        setSurname(arr[1]);
        setPosition(arr[2]);
        setAddress(arr[3]);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}