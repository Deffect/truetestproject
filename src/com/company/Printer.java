package com.company;

public class Printer {
    private int id;
    private String model;
    private int cartridgeNumber;
    private String printerType;
    private String procreator;

    public Printer(String strData) {
        String[] arr = strData.split(" ");
        setId(Integer.parseInt(arr[0]));
        setModel(arr[1]);
        setCartridgeNumber(Integer.parseInt(arr[2]));
        setPrinterType(arr[3]);
        setProcreator(arr[4]);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCartridgeNumber() {
        return cartridgeNumber;
    }

    public void setCartridgeNumber(int cartridgeNumber) {
        this.cartridgeNumber = cartridgeNumber;
    }

    public String getPrinterType() {
        return printerType;
    }

    public void setPrinterType(String printerType) {
        this.printerType = printerType;
    }

    public String getProcreator() {
        return procreator;
    }

    public void setProcreator(String procreator) {
        this.procreator = procreator;
    }
}
