package com.company;

public class Monitor {
    private int id;
    private String resolution;
    private int frequency;
    private String matrixType; //tn,ips,pva
    private String interfaceConnectors; //"HDMI,VGA,DVI"
    private String procreator;

    public Monitor(String strData) {
        String[] arr = strData.split(" ");
        setId(Integer.parseInt(arr[0]));
        setResolution(arr[1]);
        setFrequency(Integer.parseInt(arr[2]));
        setMatrixType(arr[3]);
        setInterfaceConnectors(arr[4]);
        setProcreator(arr[5]);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getMatrixType() {
        return matrixType;
    }

    public void setMatrixType(String matrixType) {
        this.matrixType = matrixType;
    }

    public String getInterfaceConnectors() {
        return interfaceConnectors;
    }

    public void setInterfaceConnectors(String interfaceConnectors) {
        this.interfaceConnectors = interfaceConnectors;
    }

    public String getProcreator() {
        return procreator;
    }

    public void setProcreator(String procreator) {
        this.procreator = procreator;
    }
}
