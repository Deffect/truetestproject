package com.company;

public class PC {
    private int id;
    private String body;
    private String motherboard;
    private String PowerSupply;
    private String ram;
    private String graphicsCard;
    private String hdd;
    private String procreator;

    public PC(String strData) {
        String[] arr = strData.split(" ");
        setId(Integer.parseInt(arr[0]));
        setBody(arr[1]);
        setMotherboard(arr[2]);
        setPowerSupply(arr[3]);
        setRam(arr[4]);
        setGraphicsCard(arr[5]);
        setHdd(arr[6]);
        setProcreator(arr[7]);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(String motherboard) {
        this.motherboard = motherboard;
    }

    public String getPowerSupply() {
        return PowerSupply;
    }

    public void setPowerSupply(String powerSupply) {
        PowerSupply = powerSupply;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getGraphicsCard() {
        return graphicsCard;
    }

    public void setGraphicsCard(String graphicsCard) {
        this.graphicsCard = graphicsCard;
    }

    public String getHdd() {
        return hdd;
    }

    public void setHdd(String hdd) {
        this.hdd = hdd;
    }

    public String getProcreator() {
        return procreator;
    }

    public void setProcreator(String procreator) {
        this.procreator = procreator;
    }
}
