package com.company;

import com.company.storage.FileDataReader;
import com.company.storage.MonitorsDataStorage;
import com.company.storage.PCDataStorage;
import com.company.storage.PrintersDataStorage;
import com.company.storage.UsersDataStorage;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        FileDataReader monitorsDataStorage = new MonitorsDataStorage();
        FileDataReader pcDataStorage = new PCDataStorage();
        FileDataReader printersDataStorage = new PrintersDataStorage();
        FileDataReader usersDataStorage = new UsersDataStorage();

        Scanner scan = new Scanner(System.in);
        int input = 0;
        while (input < 6) {

            System.out.println("1. Common list; 2. Monitor list; 3. PC list; 4. Printer list; 5. Users list; 6. Exit");
            input = scan.nextInt();

            switch (input) {
                case 1:
                    monitorsDataStorage.readFile();
                    pcDataStorage.readFile();
                    printersDataStorage.readFile();
                    usersDataStorage.readFile();
                    break;
                case 2:
                    monitorsDataStorage.readFile();
                    break;
                case 3:
                    pcDataStorage.readFile();
                    break;
                case 4:
                    printersDataStorage.readFile();
                    break;
                case 5:
                    usersDataStorage.readFile();
                    break;
                case 6:
                    System.out.println("exit");
                    return;
                default:
                    System.out.println("Wrong input! Please enter values from the list");
                    input = 1;
            }
        }
    }
}