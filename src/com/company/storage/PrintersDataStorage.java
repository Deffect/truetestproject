package com.company.storage;

import com.company.Printer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PrintersDataStorage implements FileDataReader {

    public PrintersDataStorage() {
    }

    @Override
    public void readFile() throws FileNotFoundException {
        File file = new File("printerDataStorage.txt");
        Scanner scanPrinter = new Scanner(file);
        while (scanPrinter.hasNextLine()) {
            String voidStr = scanPrinter.nextLine();
            if (!"".equals(voidStr)) {
                String[] splits = voidStr.split("\n");
                Printer[] printers = new Printer[splits.length];
                for (int i = 0; i < splits.length; i++) {
                    printers[i] = new Printer(splits[i]);
                }
                for (Printer printer : printers) {
                    printInfo(printer);
                }
            } else
                break;
        }
        scanPrinter.close();
    }

    public void printInfo(Printer printer) {
        System.out.println(
                "Инв.номер - " + printer.getId() + " Модель - hp " + printer.getModel() +
                        " Номер картриджа - " + printer.getCartridgeNumber() +
                        " Тип принтера - " + printer.getPrinterType() + " Производитель - " + printer.getId());
    }

}
