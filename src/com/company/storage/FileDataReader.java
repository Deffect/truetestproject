package com.company.storage;

import java.io.FileNotFoundException;

public interface FileDataReader {

    void readFile() throws FileNotFoundException;
}
