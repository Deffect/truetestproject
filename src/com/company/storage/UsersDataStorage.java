package com.company.storage;

import com.company.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class UsersDataStorage implements FileDataReader {

    public UsersDataStorage() {
    }

    @Override
    public void readFile() throws FileNotFoundException {
        File file = new File("userDataStorage.txt");
        Scanner scanUser = new Scanner(file);
        while (scanUser.hasNextLine()) {
            String voidStr = scanUser.nextLine();
            if (!"".equals(voidStr)) {
                String[] splits = voidStr.split("\n");
                User[] users = new User[splits.length];
                for (int i = 0; i < splits.length; i++) {
                    users[i] = new User(splits[i]);
                }
                for (User user : users) {
                    printInfo(user);
                }
            } else
                break;
        }
        scanUser.close();
    }

    public void printInfo(User user) {
        System.out.println(
                "Имя - " + user.getName() + " Фамилия - " + user.getSurname() +
                        " Должность - " + user.getPosition() + " Адрес - " + user.getAddress());
    }
}

