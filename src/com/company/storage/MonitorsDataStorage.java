package com.company.storage;

import com.company.Monitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MonitorsDataStorage implements FileDataReader {

    public MonitorsDataStorage() {
    }

    @Override
    public void readFile() throws FileNotFoundException {
        File file = new File("monitorDataStorage.txt");
        Scanner scanMonitor = new Scanner(file);
        while (scanMonitor.hasNextLine()) {
            String voidStr = scanMonitor.nextLine();
            if (!"".equals(voidStr)) {
                String[] splits = voidStr.split("\n");
                Monitor[] monitors = new Monitor[splits.length];
                for (int i = 0; i < splits.length; i++) {
                    monitors[i] = new Monitor(splits[i]);
                }
                for (Monitor monitor : monitors) {
                    printInfo(monitor);
                }
            } else
                break;
        }
        scanMonitor.close();
    }

    public void printInfo(Monitor monitor) {
        System.out.println(
                "Инв.номер - " + monitor.getId() + " Разрешение - " +
                        monitor.getResolution() + " Частота - " + monitor.getFrequency() +
                        " Тип матрицы - " + monitor.getMatrixType() +
                        " Интерфейсный разъём - " + monitor.getInterfaceConnectors() +
                        " Производитель - " + monitor.getProcreator());
    }
}
