package com.company.storage;

import com.company.PC;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PCDataStorage implements FileDataReader {

    public PCDataStorage() {
    }

    @Override
    public void readFile() throws FileNotFoundException {
        File file = new File("pcDataStorage.txt");
        Scanner scanPC = new Scanner(file);
        while (scanPC.hasNextLine()) {
            String voidStr = scanPC.nextLine();
            if (!"".equals(voidStr)) {
                String[] splits = voidStr.split("\n");
                PC[] pcs = new PC[splits.length];
                for (int i = 0; i < splits.length; i++) {
                    pcs[i] = new PC(splits[i]); 
                }
                for (PC pc : pcs) {
                    printInfo(pc);
                }
            } else
                break;
        }
        scanPC.close();
    }

    public void printInfo(PC pc) {
        System.out.println(
                "Инв.номер - " + pc.getId() + " Корпус - " + pc.getBody() +
                        " Мат.плата - " + pc.getMotherboard() + " Блок питания - " + pc.getPowerSupply() +
                        " RAM - " + pc.getRam() + " Видеокарта - " + pc.getGraphicsCard() +
                        " Жесткий диск - " + pc.getHdd() + " Производитель - " + pc.getProcreator());
    }
}

